mavenJob('Code-Coverage') 
description('Code Coverage test')
{scm {
        git {
            remote {
                name('CI')
                url('https://github.com/vikas-gautam/ContinuousIntegration.git')
                        }
          branch('master')
                }
        }

rootPOM('Spring3HibernateApp/pom.xml')
goals('clean cobertura:cobertura -Dcobertura.report.format=xml')
mavenInstallation('maven')
publishers {
coberturaPublisher {
     coberturaReportFile('**/target/site/cobertura/coverage.xml')
     onlyStable(false)
     failUnhealthy(false)
     failUnstable(false)
     autoUpdateHealth(false)
     autoUpdateStability(false)
     zoomCoverageChart(false)
     failNoReports(true)
     sourceEncoding('ASCII')
     maxNumberOfBuilds(0)
    }
       }
    
    }
