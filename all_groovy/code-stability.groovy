mavenJob('Code-Stability') 
description('Code Stability Test')
{scm {
        git {
            remote {
                name('CI')
                url('https://github.com/vikas-gautam/ContinuousIntegration.git')
                        }
          branch('master')
                }
        }

rootPOM('Spring3HibernateApp/pom.xml')
goals('clean compile')
mavenInstallation('maven')
    
   publishers {
        extendedEmail {
            recipientList('vikas.gautam@opstree.com')
            defaultSubject('Oops')
            defaultContent('Something broken')
            contentType('text/html')
            triggers {
                beforeBuild()
                unstable {
                    subject('Subject')
                    content('Body')
                    sendTo {
                        developers()
                        requester()
                        culprits()
                    }
                }
            }
        }
    }
}
