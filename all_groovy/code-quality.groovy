mavenJob('Code-Quality') 
description('Code Quality test')
{scm {
        git {
            remote {
                name('CI')
                url('https://github.com/vikas-gautam/ContinuousIntegration.git')
                        }
          branch('master')
                }
        }

rootPOM('Spring3HibernateApp/pom.xml')
goals('clean checkstyle:checkstyle')
publishers {
        analysisCollector {
            checkstyle()
            findbugs()
       }
